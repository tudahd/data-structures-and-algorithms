package problem1;

import java.lang.reflect.Array;

/**
 * Created by Asus on 10/10/2017.
 * Naive solution would be to consider every pair in given array and return if desired sum is found.
 * The time complexity of this solution is O(n^2) and auxiliary space used by the program is O(1)
 */
public class FindPairWithGivenSumInArray {

    public static void main(String[] args) {
        int[] input = {1,3,6,7,8,4,2};
        int sum = 6;
        findPair(input,sum);
    }

    private static void findPair(int[] input, int sum) {
        int size = input.length;
        for (int i = 0; i < size - 1; i++) {
                for (int j = i + 1; j < size; j++) {
                    if (input[i] + input[j] == sum) {
                        System.out.println(i + " " + j);
                        return;
                }
            }
        }
        System.out.println("Not found");
    }
}
